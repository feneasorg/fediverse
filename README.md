# FEDIVERSE GUIDE

### IMPORTANT: UPDATE YOUR BOOKMARKS - code has moved [to Codeberg](https://codeberg.org/fediverse/fediparty).

====

![website logo](./source/img/touch/favicon-32x32.png?raw=true) A quick look into Fediverse networks

→ [https://fediverse.party](https://fediverse.party) ←
